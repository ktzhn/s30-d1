const express = require("express");

// Require mongoose package
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// Connection to MongoDB Atlas
// SYNTAX: mongoose.connect("<MongoDB Atlas connection string>", {useNewUrlParser:true });
mongoose.connect("mongodb+srv://dbKristenTan:APTcxPmkgr05VmxW@wdc028-course-booking.mkooj.mongodb.net/b138_to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

let db = mongoose.connection;

// If a connection error occured, output in the console
// console.error.bind(console) allow us to print errors in the browser console in the terminal
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database"));

// Schema: determines the structure of the documents to be written in the database
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});

// Create the Task model

const Task = mongoose.model("Task", taskSchema);

app.use(express.json());

app.use(express.urlencoded({extended:true}));

// Create a POST route to create a new task
app.post("/tasks", (req, res) => {
	Task.findOne({name: req.body.name}, (err, result) => {
		// If a document was found and the document's name mathces the information sent via the client/postman
		if(result !=null && result.name == req.body.name){
			return res.send("Duplicate task found");
		}
		else{
			let newTask = new Task({
				name : req.body.name
			});

			newTask.save((saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("New task created")
				}
			})
		}
	})
})

// Create a GET request to retrieve all the tasks
app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		// If an error occured
		if(err){
			return console.log(err);
		}
		// If no errors are found
		else{
			return res.status(200).json({
				data : result
			})
		}
	})
})


// 1. Create a User schema.
const userSchema = new mongoose.Schema({
	username: String,
	password: String
});
// 2. Create a User model.
const User = mongoose.model("User", userSchema);
// 3. Create a POST route that will access the "/signup" route that will create a user.
// 4. Process a POST request at the "/users" route using postman to register a user.
app.post("/signup", (req, res) => {
	User.findOne({username: req.body.username}, (err, result) => {
		if(result !=null && result.username == req.body.username){
			return res.send("User already exists! Please choose another name.");
		}
		else{
			let newUser = new User({
				username : req.body.username,
				password: req.body.password
			});

			newUser.save((saveErr, savedUser) => {
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("User account successfully registered!")
				}
			})
		}
	})
})

app.get("/users", (req, res) => {
	User.find({}, (err, result) => {
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).json({
				data : result
			})
		}
	})
})




app.listen(port, () => console.log(`Server is running at port ${port}`));